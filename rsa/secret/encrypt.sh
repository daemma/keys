#!  /usr/bin/env bash
### ############################################################################
##! @file       encrypt.sh 
##! @brief      Symmetrically encrypt secret information
##! @author     0xD62EE11516877AA8
##! @date       2016-09-17
##! @copyright  GPLv3+

__files="sec.gpg sub.gpg rev.asc keybase-paperkey.txt"  ## secret files 
__clear="secret.tgz"       ## Zipped tar-ball of secrets
__cypher="${__clear}.gpg"  ## Encrypted zipped tar-ball of secrets

## Bundle secret data
tar --verbose --create --gzip --file "${__clear}" ${__files}

if [ -f "${__clear}" ] ; then
    ## Symmetrically encrypt secret data, with forced-tinfoil-hat options
    ## Use symmetric encryption since private keys may have been lost.
    gpg --output "${__cypher}" --local-user 0xD62EE11516877AA8 \
	--sign --symmetric \
	--cipher-algo AES256 \
	--s2k-cipher-algo AES256 --s2k-digest-algo SHA512 \
	--s2k-mode 3 --s2k-count 65011712 \
	"${__clear}"

    ## Obliterate the clear-ish secret data, with forced-tinfoil-hat options
    shred --verbose --force --zero --remove --iterations=5 \
	  "${__clear}" ${__files}
else
    echo "Clear-text file ${__clear} not found!"
fi

### end encrypt.sh
### ############################################################################
