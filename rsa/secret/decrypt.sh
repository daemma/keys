#!  /usr/bin/env bash
### ############################################################################
##! @file       decrypt.sh
##! @brief      Decrypt secret information
##! @author     0xD62EE11516877AA8
##! @date       2016-09-17
##! @copyright  GPLv3+

__clear="secret.tgz"       ## Zipped tar-ball of secrets
__cypher="${__clear}.gpg"  ## Encrypted zipped tar-ball of secrets

## Decrypt the secret data
gpg --output  "${__clear}" --decrypt "${__cypher}"

if [ -f "${__clear}" ] ; then
    ## Unpack the secret data
    tar --verbose --extract --gzip --file "${__clear}"

    ## Obliterate the clear-ish secret data bundle,
    ## with forced-tinfoil-hat options
    shred --verbose --force --zero --remove --iterations=5 "${__clear}" 
else
    echo "Clear-text file ${__clear} not found!"
fi

### end decrypt.sh
### ############################################################################
