<!--#########################################################################-->
<!-- @file       Readme.md -->
<!-- @brief      Readme file for keys repository. -->
<!-- @author     0xD62EE11516877AA8 -->
<!-- @date       2016-09-17 -->
<!-- @copyright  GPLv3+ -->

# Keys
This repository contains public information about my cryptographic keys, 
including:
  - [Key Policies](https://gitlab.com/daemma/keys/blob/master/policy.md): 
	to help you decide whether, or to what degree, to trust my keys.
  - [RSA Public Key](https://gitlab.com/daemma/keys/raw/master/rsa/0xD62EE11516877AA8.asc):
	 - `99DB AE95 5566 2741 A00D  F7AF D62E E115 1687 7AA8`
	 - [keybase.io](https://keybase.io/daemma)

<!--end Readme.md -->
<!--#########################################################################-->
